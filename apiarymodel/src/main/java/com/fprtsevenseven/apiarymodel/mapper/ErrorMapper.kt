package com.fprtsevenseven.apiarymodel.mapper

import com.fprtsevenseven.basemodel.data.GeneralModelException
import com.fprtsevenseven.basemodel.data.GeneralServerException
import com.fprtsevenseven.basemodel.data.NetworkApiException
import java.io.IOException

object ErrorMapper {

    fun mapException(t: Throwable): Throwable {
        return when (t) {
            is IOException -> NetworkApiException()
            is GeneralModelException, is GeneralServerException -> t
            else -> GeneralModelException(t.message ?: "Unknown exception")
        }
    }

}