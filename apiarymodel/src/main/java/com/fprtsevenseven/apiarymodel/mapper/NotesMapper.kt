package com.fprtsevenseven.apiarymodel.mapper

import com.fprtsevenseven.apiarymodel.data.Note
import com.fprtsevenseven.basemodel.data.NoteModel

object NotesMapper {

    fun mapNotesToModel(notes: List<Note>) : List<NoteModel> =
        notes.map {
            mapNoteToModel(it)
        }

    fun mapNoteToModel(note: Note) : NoteModel =
        NoteModel(
            id = note.id.toString(),
            content = note.title ?: ""
        )

}