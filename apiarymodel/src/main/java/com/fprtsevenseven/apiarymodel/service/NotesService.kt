package com.fprtsevenseven.apiarymodel.service

import com.fprtsevenseven.apiarymodel.data.Note
import retrofit2.Call
import retrofit2.http.*

interface NotesService {
    @Headers("Content-Type: application/json")
    @GET("/notes")
    fun getNotes()
            : Call<List<Note>>

    @Headers("Content-Type: application/json")
    @POST("/notes")
    fun createNote(@Body request: Note)
            : Call<Note>

    @Headers("Content-Type: application/json")
    @GET("/notes/{id}")
    fun getNote(@Path("id") id: String)
            : Call<Note>

    @Headers("Content-Type: application/json")
    @PUT("/notes/{id}")
    fun updateNote(@Path("id") id: String, @Body request: Note)
            : Call<Note>

    @Headers("Content-Type: application/json")
    @DELETE("/notes/{id}")
    fun deleteNote(@Path("id") id: String)
            : Call<Void>

}