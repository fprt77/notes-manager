package com.fprtsevenseven.apiarymodel.data

data class Note (
        val id: Int?,
        val title: String?
)