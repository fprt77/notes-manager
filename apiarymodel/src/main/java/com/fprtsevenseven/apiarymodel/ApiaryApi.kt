package com.fprtsevenseven.apiarymodel

import com.fprtsevenseven.apiarymodel.data.Note
import com.fprtsevenseven.apiarymodel.mapper.ErrorMapper
import com.fprtsevenseven.apiarymodel.mapper.NotesMapper
import com.fprtsevenseven.apiarymodel.service.NotesService
import com.fprtsevenseven.basemodel.NotesApi
import com.fprtsevenseven.basemodel.await
import com.fprtsevenseven.basemodel.awaitVoid
import com.fprtsevenseven.basemodel.data.NoteModel
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Suppress("unused")
class ApiaryApi(
        endpoint: String,
        httpLoggingInterceptor: HttpLoggingInterceptor
): NotesApi {

    private var notesService: NotesService

    init {
        val okHttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor(httpLoggingInterceptor)
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .build()
        val retrofit = Retrofit.Builder()
                .baseUrl(endpoint)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
        notesService = retrofit.create(NotesService::class.java)
    }

    override suspend fun listNotes(): List<NoteModel> = safeCall{
        val result = notesService.getNotes().await()
        return NotesMapper.mapNotesToModel(result)
    }

    override suspend fun addNote(note: NoteModel): NoteModel = safeCall {
        val postNote = Note(null, note.content)
        val result = notesService.createNote(postNote).await()
        return NotesMapper.mapNoteToModel(result)
    }

    override suspend fun getNote(id: String): NoteModel = safeCall {
        val result = notesService.getNote(id).await()
        return NotesMapper.mapNoteToModel(result)
    }

    override suspend fun updateNote(note: NoteModel): NoteModel = safeCall {
        val putNote = Note(null, note.content)
        val result = notesService.updateNote(note.id, putNote).await()
        return NotesMapper.mapNoteToModel(result)
    }

    override suspend fun deleteNote(id: String) {
        safeCall {
            notesService.deleteNote(id).awaitVoid()
        }
    }

    private inline fun <T> safeCall(callBlock: () -> T): T {
        try {
            return callBlock()
        } catch (t: Throwable) {
            throw ErrorMapper.mapException(t)
        }
    }

}