package com.fprtsevenseven.notes

import com.fprtsevenseven.apiarymodel.ApiaryApi
import com.fprtsevenseven.basemodel.data.NoteModel
import kotlinx.coroutines.*
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Test

import org.junit.Assert.*

/**
 * Tests to verify Retrofit calls to mocked APIARY api.
 */
class ApiUnitTest {

    private val api = ApiaryApi(
        endpoint = "https://private-anon-b9802254ca-note10.apiary-mock.com",
        httpLoggingInterceptor = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
    )

    @Test
    fun getNotes_isCorrect() = checkedCall {
        api.listNotes()
    }

    @Test
    fun getNote_isCorrect() = checkedCall {
        val id = "1"
        val result = api.getNote(id)
        assertNotNull(result.id)
        // this will not pass with mock
//        assertEquals(id, result.id)
    }

    @Test
    fun createNote_isCorrect() = checkedCall {
        val note = NoteModel(content = "Note message")
        val result = api.addNote(note)
        assertNotNull(result.id)
        assert(result.id.isNotEmpty())
        // this will not pass with mock
//            assertEquals(note.content, result.content)
    }

    @Test
    fun updateNote_isCorrect() = checkedCall {
        val note = NoteModel(id = "1", content = "Note message")
        val result = api.updateNote(note)
        assertNotNull(result.id)
        assert(result.id.isNotEmpty())
        // this will not pass with mock
//            assertEquals(note.content, result.content)
    }

    @Test
    fun deleteNote_isCorrect() = checkedCall {
        val id = "1"
        api.deleteNote(id)
    }

    private fun checkedCall(callBlock: suspend CoroutineScope.() -> Unit) = runBlocking {
        var checkedThrowable: Throwable? = null
        val job = CoroutineScope(Dispatchers.IO).launch {
            try {
                callBlock.invoke(this)
            } catch (t: Throwable) {
                checkedThrowable = t
            }
        }
        job.join()
        checkedThrowable?.let { fail(it.message) } ?: Unit
    }
}
