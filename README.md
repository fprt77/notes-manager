### Version ###
0.0.2

# Notes Manager #

## Summary ##

App for managing your personal notes.

Available operations:

* list all notes
* create new note
* edit note
* delete note

## Flavors ##

### Apiary ###

App will target https://note10.docs.apiary.io/#reference/notes/note API.

To change endpoint replace url in projectDir\app\src\apiary\java\com\fprtsevenseven\notes\Flavor.kt

### Room ###

App will target Room database. All notes will be saved locally on device.

### Mock ###

App will target mocked API. None of the values will be persisted. Purpose is just to test app in isolation.