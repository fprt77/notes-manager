package com.fprtsevenseven.roommodel.test

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.fprtsevenseven.basemodel.data.NoteModel
import com.fprtsevenseven.roommodel.RoomApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith

/**
 * Test to verify correct behavior of RoomApi
 */
@RunWith(AndroidJUnit4::class)
class RoomApiUnitTest {

    private lateinit var api: RoomApi

    @Before
    fun init() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        api = RoomApi(context)
    }

    @Test
    fun checkRoomFlow_isCorrect() = checkedCall {
        var notes = api.listNotes()
        assert(notes.isEmpty())

        val note1 = NoteModel("", "Note 1")
        val addedNote = api.addNote(note1)
        assertEquals(note1.content, addedNote.content)
        note1.id = addedNote.id

        notes = api.listNotes()
        assert(notes.isNotEmpty())
        assertEquals(note1.content, notes.first().content)

        note1.content = "Note 1 updated"
        val updated = api.updateNote(note1)
        assertEquals(note1.id, updated.id)
        assertEquals(note1.content, updated.content)

        val noteFromList = api.getNote(note1.id)
        assertEquals(note1.id, noteFromList.id)
        assertEquals(note1.content, noteFromList.content)

        notes = api.listNotes()
        assertEquals(1, notes.size)

        api.deleteNote(note1.id)

        notes = api.listNotes()
        assert(notes.isEmpty())
    } ?: Unit

    private fun checkedCall(callBlock: suspend CoroutineScope.() -> Unit) = runBlocking {
        var checkedThrowable: Throwable? = null
        val job = CoroutineScope(Dispatchers.IO).launch {
            try {
                callBlock.invoke(this)
            } catch (t: Throwable) {
                checkedThrowable = t
            }
        }
        job.join()
        checkedThrowable?.let { fail(it.message) }
    }
}