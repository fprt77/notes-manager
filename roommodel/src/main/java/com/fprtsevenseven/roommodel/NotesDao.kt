package com.fprtsevenseven.roommodel

import androidx.room.*

@Dao
interface NotesDao {

    @Query("SELECT * FROM ${NoteRoomItem.TABLE_NAME}")
    fun getAll(): List<NoteRoomItem>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(roomItem: NoteRoomItem): Long

    @Query("SELECT * FROM ${NoteRoomItem.TABLE_NAME} WHERE ${NoteRoomItem.COLUMN_ID} = :id")
    fun getById(id: Long): List<NoteRoomItem>

    @Query("DELETE FROM ${NoteRoomItem.TABLE_NAME} WHERE ${NoteRoomItem.COLUMN_ID} = :id")
    fun deleteById(id: Long): Int

    @Update
    fun update(roomItem: NoteRoomItem): Int
}