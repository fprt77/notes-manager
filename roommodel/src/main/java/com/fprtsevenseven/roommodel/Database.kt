package com.fprtsevenseven.roommodel

import androidx.room.Database
import androidx.room.Entity
import androidx.room.RoomDatabase

@Entity
@Database(entities = [NoteRoomItem::class], version = 1, exportSchema = false)
abstract class Database: RoomDatabase() {
    abstract fun notesDao(): NotesDao
}