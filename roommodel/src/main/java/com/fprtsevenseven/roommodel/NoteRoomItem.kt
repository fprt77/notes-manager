package com.fprtsevenseven.roommodel

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fprtsevenseven.basemodel.data.NoteModel
import java.lang.NumberFormatException

@Entity(tableName = NoteRoomItem.TABLE_NAME)
data class NoteRoomItem (

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = COLUMN_ID)
    var id: Int,

    @ColumnInfo(name = COLUMN_CONTENT)
    var content: String

) {
    companion object {
        const val TABLE_NAME = "notes"

        const val COLUMN_ID = "id"
        const val COLUMN_CONTENT = "content"

        fun fromNoteModel(noteModel: NoteModel): NoteRoomItem {
            val id = try {
                noteModel.id.toInt()
            } catch (e: NumberFormatException) {
                0
            }
            return NoteRoomItem(id, noteModel.content)
        }
    }

    fun getNoteModel(): NoteModel = NoteModel(id.toString(), content)
}