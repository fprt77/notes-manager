package com.fprtsevenseven.roommodel

import android.content.Context
import androidx.room.Room
import com.fprtsevenseven.basemodel.NotesApi
import com.fprtsevenseven.basemodel.data.GeneralModelException
import com.fprtsevenseven.basemodel.data.NoteModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Suppress("unused")
class RoomApi(context: Context): NotesApi {

    private var counter = 1
    private var notesDao: NotesDao = Room
        .databaseBuilder(context.applicationContext, Database::class.java, "database")
        .build()
        .notesDao()

    override suspend fun listNotes(): List<NoteModel> = withContext(Dispatchers.IO) {
        notesDao.getAll().map { it.getNoteModel() }
    }

    override suspend fun addNote(note: NoteModel): NoteModel = withContext(Dispatchers.IO) {
        note.id = ""
        val id = notesDao.insert(NoteRoomItem.fromNoteModel(note))
        notesDao.getById(id).firstOrNull()?.getNoteModel() ?: throw GeneralModelException("Database error")
    }

    override suspend fun getNote(id: String): NoteModel = withContext(Dispatchers.IO) {
        notesDao.getById(id.toLong()).firstOrNull()?.getNoteModel() ?: throw GeneralModelException("Note not found")
    }

    override suspend fun updateNote(note: NoteModel): NoteModel = withContext(Dispatchers.IO) {
        val updated = notesDao.update(NoteRoomItem.fromNoteModel(note))
        if (updated > 0) {
            notesDao.getById(note.id.toLong()).firstOrNull()?.getNoteModel()
                ?: throw GeneralModelException("Database error")
        } else {
            throw GeneralModelException("Note not found")
        }
    }

    override suspend fun deleteNote(id: String) = withContext(Dispatchers.IO) {
        notesDao.deleteById(id.toLong())
        Unit
    }

}