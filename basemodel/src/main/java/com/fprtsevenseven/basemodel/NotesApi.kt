package com.fprtsevenseven.basemodel

import com.fprtsevenseven.basemodel.data.NoteModel

interface NotesApi {

    suspend fun listNotes(): List<NoteModel>
    suspend fun addNote(note: NoteModel): NoteModel
    suspend fun getNote(id: String): NoteModel
    suspend fun updateNote(note: NoteModel): NoteModel
    suspend fun deleteNote(id: String)

}