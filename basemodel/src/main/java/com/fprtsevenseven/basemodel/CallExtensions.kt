package com.fprtsevenseven.basemodel

import com.fprtsevenseven.basemodel.data.GeneralModelException
import com.fprtsevenseven.basemodel.data.GeneralServerException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

suspend fun <T> Call<T>.await(): T = suspendCancellableCoroutine { continuation ->
    continuation.invokeOnCancellation { cancel() }
    this.enqueue(object : Callback<T> {
        override fun onResponse(call: Call<T>, response: Response<T>) {
            val body = response.body()
            when {
                response.isSuccessful && body != null -> continuation.resume(body)
                response.isSuccessful -> continuation.cancel()
                !response.isSuccessful -> continuation.resumeWithException(
                    GeneralServerException(
                        httpCode = response.code(),
                        errorCode = 0,
                        message = response.message()
                    )
                )
                else -> continuation.resumeWithException(
                    GeneralModelException("No result received from ${call.request().url()}")
                )
            }
        }

        override fun onFailure(call: Call<T>, t: Throwable) {
            continuation.resumeWithException(t)
        }
    })
}

suspend fun Call<Void>.awaitVoid(): Unit = suspendCancellableCoroutine { continuation ->
    continuation.invokeOnCancellation { cancel() }
    this.enqueue(object : Callback<Void> {
        override fun onResponse(call: Call<Void>, response: Response<Void>) {
            when {
                response.isSuccessful -> continuation.resume(Unit)
                !response.isSuccessful -> continuation.resumeWithException(
                    GeneralServerException(
                        httpCode = response.code(),
                        errorCode = 0,
                        message = response.message()
                    )
                )
            }
        }

        override fun onFailure(call: Call<Void>, t: Throwable) {
            continuation.resumeWithException(t)
        }
    })
}