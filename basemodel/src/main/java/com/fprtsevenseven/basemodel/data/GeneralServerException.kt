package com.fprtsevenseven.basemodel.data

class GeneralServerException(
    val httpCode: Int,
    val errorCode: Int,
    message: String
): Throwable(message)