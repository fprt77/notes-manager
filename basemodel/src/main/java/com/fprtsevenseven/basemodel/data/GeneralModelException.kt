package com.fprtsevenseven.basemodel.data

class GeneralModelException(
    message: String
): Throwable(message)