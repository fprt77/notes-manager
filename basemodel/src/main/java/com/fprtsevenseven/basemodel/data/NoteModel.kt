package com.fprtsevenseven.basemodel.data

data class NoteModel(
        var id: String = "",
        var content: String = ""
)