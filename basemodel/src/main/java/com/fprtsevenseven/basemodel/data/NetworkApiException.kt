package com.fprtsevenseven.basemodel.data

class NetworkApiException: Throwable("Network error")