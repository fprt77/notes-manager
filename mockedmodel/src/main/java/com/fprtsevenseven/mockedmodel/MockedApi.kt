package com.fprtsevenseven.mockedmodel

import com.fprtsevenseven.basemodel.NotesApi
import com.fprtsevenseven.basemodel.data.GeneralModelException
import com.fprtsevenseven.basemodel.data.NoteModel

@Suppress("unused")
class MockedApi: NotesApi {

    private val notes = ArrayList<NoteModel>()
    private var counter = 1

    override suspend fun listNotes(): List<NoteModel> = ArrayList(notes)

    override suspend fun addNote(note: NoteModel): NoteModel {
        val noteToAdd = NoteModel(counter.toString(), note.content)
        counter++
        notes.add(noteToAdd)
        return noteToAdd
    }

    override suspend fun getNote(id: String): NoteModel {
        return notes.find { it.id == id } ?: throw GeneralModelException("Note not found")
    }

    override suspend fun updateNote(note: NoteModel): NoteModel {
        return notes.find { it.id == note.id }?.apply {
            content = note.content
        } ?: throw GeneralModelException("Note not found")
    }

    override suspend fun deleteNote(id: String) {
        notes.removeAll { it.id == id }
    }

}