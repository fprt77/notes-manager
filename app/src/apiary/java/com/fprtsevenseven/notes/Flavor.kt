package com.fprtsevenseven.notes

import android.content.Context
import com.fprtsevenseven.apiarymodel.ApiaryApi
import com.fprtsevenseven.basemodel.NotesApi
import okhttp3.logging.HttpLoggingInterceptor

class Flavor: FlavorBase() {

    override fun createApi(context: Context): NotesApi = ApiaryApi(
        endpoint = "http://private-9aad-note10.apiary-mock.com/",
        httpLoggingInterceptor = HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        }
    )

}