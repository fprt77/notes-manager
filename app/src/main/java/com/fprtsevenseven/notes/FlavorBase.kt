package com.fprtsevenseven.notes

import android.content.Context
import com.fprtsevenseven.basemodel.NotesApi

abstract class FlavorBase {

    abstract fun createApi(context: Context): NotesApi

}