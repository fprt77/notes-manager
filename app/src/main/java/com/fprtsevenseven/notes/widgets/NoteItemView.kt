package com.fprtsevenseven.notes.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import com.fprtsevenseven.notes.R
import com.fprtsevenseven.notes.data.ExpandingNoteModel
import com.fprtsevenseven.notes.databinding.NoteItemBinding
import com.fprtsevenseven.notes.viewmodels.NotesItemViewModel

class NoteItemView: ConstraintLayout {
    constructor(context: Context) : super(context) { init()
    }
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) { init()
    }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) { init()
    }

    lateinit var binding : NoteItemBinding

    private fun init() {
        binding = DataBindingUtil.inflate<NoteItemBinding>(
            LayoutInflater.from(context),
            R.layout.note_item,
            this,
            true
        ).apply {
            viewModel = NotesItemViewModel()
            lifecycleOwner = context as LifecycleOwner
        }
    }

    fun bindNote(note: ExpandingNoteModel) {
        binding.viewModel?.expandingNote?.value = note
    }
}