package com.fprtsevenseven.notes.viewmodels

import android.view.View
import androidx.lifecycle.*

class MainActivityViewModel : ViewModel() {

    val loadingVisibility = MutableLiveData<Int>().apply { value = View.GONE }

}