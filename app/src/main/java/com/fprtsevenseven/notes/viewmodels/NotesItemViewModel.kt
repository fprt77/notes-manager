package com.fprtsevenseven.notes.viewmodels

import android.view.View
import android.widget.TextView
import androidx.lifecycle.*
import com.fprtsevenseven.notes.data.ExpandingNoteModel
import com.fprtsevenseven.notes.utils.canBeSingleLine
import com.fprtsevenseven.notes.utils.SingleLiveEvent

class NotesItemViewModel : ViewModel() {

    val expandingNote = MutableLiveData<ExpandingNoteModel>()
    val title: LiveData<String>
    val text: LiveData<String>
    private val expanded = MediatorLiveData<Boolean>().apply { value = false }
    val iconResource: LiveData<Int>
    val maxLines: LiveData<Int>
    private val height: LiveData<Int>
    val deleteNoteClick = SingleLiveEvent<Void>()
    val editNoteClick = SingleLiveEvent<Void>()
    val expandVisibility: LiveData<(TextView) -> Int>

    init {
        title = Transformations.map(expandingNote) {
            it.noteModel.id
        }
        text = Transformations.map(expandingNote) {
            it.noteModel.content
        }
        expandVisibility = Transformations.map(expandingNote) {
            getExpandVisibility()
        }
        expanded.addSource(expandingNote) {
            expanded.value = it.expanded
        }
        iconResource = Transformations.map(expanded) {
            if (it) {
                android.R.drawable.ic_menu_close_clear_cancel
            } else {
                android.R.drawable.ic_menu_more
            }
        }
        maxLines = Transformations.map(expanded) {
            if (it) {
                Integer.MAX_VALUE
            } else {
                1
            }
        }
        height = Transformations.map(expanded) {
            if (it) {
                Int.MAX_VALUE
            } else {
                100
            }
        }
    }

    fun onExpandNoteClick() = View.OnClickListener {
        expandingNote.value = expandingNote.value?.apply { expanded = !expanded }
    }

    fun onDeleteNoteClick() = View.OnClickListener {
        deleteNoteClick.call()
    }

    fun onEditNoteClick() = View.OnClickListener {
        editNoteClick.call()
    }

    private fun getExpandVisibility(): (TextView) -> Int = { textView: TextView ->
        if (textView.canBeSingleLine(expandingNote.value?.noteModel?.content ?: "")) View.GONE else View.VISIBLE
    }
}