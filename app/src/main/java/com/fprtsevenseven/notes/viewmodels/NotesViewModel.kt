package com.fprtsevenseven.notes.viewmodels

import android.view.View
import androidx.lifecycle.*
import com.fprtsevenseven.basemodel.data.NoteModel
import com.fprtsevenseven.notes.data.ExpandingNoteModel
import com.fprtsevenseven.notes.data.NotesRepository
import com.fprtsevenseven.notes.utils.SingleLiveEvent

class NotesViewModel(
    private val notesRepository: NotesRepository
) : ViewModel() {

    val notes = MutableLiveData<List<NoteModel>>().apply { notesRepository.getNotes(this) }
    val newNoteClick = SingleLiveEvent<Void>()

    fun refresh() {
        notesRepository.getNotes(notes)
    }

    fun deleteAll() {
        notesRepository.deleteAllNotes(notes)
    }

    fun addNoteClick() = View.OnClickListener {
        newNoteClick.call()
    }

    fun createNote(text: String) {
        notesRepository.createNote(text, notes)
    }

    fun delete(id: String) {
        notesRepository.deleteNote(id, notes)
    }

    fun updateNote(expandingNoteModel: ExpandingNoteModel, newContent: String) {
        notesRepository.updateNote(expandingNoteModel.noteModel.id, newContent, notes)
    }
}