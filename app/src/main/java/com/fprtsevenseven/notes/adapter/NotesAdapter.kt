package com.fprtsevenseven.notes.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fprtsevenseven.notes.R
import com.fprtsevenseven.notes.data.ExpandingNoteModel
import com.fprtsevenseven.notes.widgets.NoteItemView
import com.fprtsevenseven.notes.utils.SingleLiveEvent

class NotesAdapter(
    private val itemsCallback: ItemsCallback
) :
    ListAdapter<ExpandingNoteModel, NotesAdapter.NoteViewHolder>(NotesDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val noteItemView = LayoutInflater.from(parent.context).inflate(
            R.layout.note_item_for_viewholder,
            parent,
            false) as NoteItemView
        return NoteViewHolder(noteItemView)
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.noteItemView.bindNote(getItem(position))
        holder.noteItemView.binding.viewModel?.deleteNoteClick?.let {
            itemsCallback.registerDeleteObserver(
                getItem(position),
                it
            )
        }
        holder.noteItemView.binding.viewModel?.editNoteClick?.let {
            itemsCallback.registerEditObserver(
                getItem(position),
                it
            )
        }
    }

    fun collapseAll() {
        for (index in 0 until itemCount) {
            getItem(index).expanded = false
        }
        notifyDataSetChanged()
    }

    fun expandAll() {
        for (index in 0 until itemCount) {
            getItem(index).expanded = true
        }
        notifyDataSetChanged()
    }

    class NoteViewHolder(
        val noteItemView: NoteItemView
    ) : RecyclerView.ViewHolder(noteItemView)

    interface ItemsCallback {
        fun registerDeleteObserver(expandingNoteModel: ExpandingNoteModel, deleteNoteClick: SingleLiveEvent<Void>)
        fun registerEditObserver(expandingNoteModel: ExpandingNoteModel, editeNoteClick: SingleLiveEvent<Void>)
    }

    class NotesDiffCallback : DiffUtil.ItemCallback<ExpandingNoteModel>() {

        override fun areItemsTheSame(
            oldItem: ExpandingNoteModel,
            newItem: ExpandingNoteModel
        ): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(
            oldItem: ExpandingNoteModel,
            newItem: ExpandingNoteModel
        ): Boolean {
            return oldItem.noteModel.content == newItem.noteModel.content
        }
    }

}