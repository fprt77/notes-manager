package com.fprtsevenseven.notes.ui

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fprtsevenseven.notes.R
import com.fprtsevenseven.notes.databinding.ActivityMainBinding
import com.fprtsevenseven.notes.utils.InjectorUtils
import com.fprtsevenseven.notes.utils.Loader
import com.fprtsevenseven.notes.viewmodels.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), Loader.LoadingContract {

    private lateinit var binding: ActivityMainBinding

    private val handler = Handler()
    private val showProgressRunnable = Runnable {
        binding.viewModel?.apply {
            loadingVisibility.value = View.VISIBLE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val factory = InjectorUtils.provideMainActivityViewModelFactory()
        val viewModelFromFactory = ViewModelProviders.of(this, factory).get(MainActivityViewModel::class.java)

        binding = DataBindingUtil.setContentView<ActivityMainBinding>(
            this,
            R.layout.activity_main
        ).apply {
            viewModel = viewModelFromFactory
            lifecycleOwner = this@MainActivity
        }
        setSupportActionBar(toolbar)

        binding.viewModel?.loadingVisibility?.observe(this, Observer {
            Log.d("Test", "Loading changed $it")
        })
    }

    override fun onResume() {
        super.onResume()
        Loader.loadingContract = this
    }

    override fun onPause() {
        Loader.loadingContract = null
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_refresh -> {
                (supportFragmentManager.findFragmentById(R.id.notes_fragment) as? NotesFragment)?.refresh()
            }
            R.id.action_collapse_all -> {
                (supportFragmentManager.findFragmentById(R.id.notes_fragment) as? NotesFragment)?.collapseAll()
            }
            R.id.action_expand_all -> {
                (supportFragmentManager.findFragmentById(R.id.notes_fragment) as? NotesFragment)?.expandAll()
            }
            R.id.action_delete_all -> {
                val builder = AlertDialog.Builder(this)
                builder.setTitle(builder.context.resources.getString(R.string.dialog_title_delete_all))
                builder.setMessage(builder.context.resources.getString(R.string.dialog_message_delete_all))
                builder.setNegativeButton(builder.context.resources.getString(R.string.dialog_button_cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                builder.setPositiveButton(builder.context.resources.getString(R.string.dialog_button_delete_all)) { dialog, _ ->
                    (supportFragmentManager.findFragmentById(R.id.notes_fragment) as? NotesFragment)?.deleteAll()
                    dialog.dismiss()
                }
                builder.create().show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setLoading(visible: Boolean, delay: Long) {
        if (visible) {
            if (delay > 0) {
                handler.postDelayed(showProgressRunnable, delay)
            } else {
                showProgressRunnable.run()
            }
        } else {
            handler.removeCallbacks(showProgressRunnable)
            binding.viewModel?.loadingVisibility?.postValue(View.GONE)
        }
    }

    override fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

}
