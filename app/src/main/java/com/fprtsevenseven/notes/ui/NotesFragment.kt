package com.fprtsevenseven.notes.ui

import android.app.AlertDialog
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.FrameLayout
import androidx.core.view.setPadding
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.fprtsevenseven.notes.R
import com.fprtsevenseven.notes.adapter.NotesAdapter
import com.fprtsevenseven.notes.data.ExpandingNoteModel
import com.fprtsevenseven.notes.databinding.FragmentNotesBinding
import com.fprtsevenseven.notes.utils.InjectorUtils
import com.fprtsevenseven.notes.viewmodels.NotesViewModel
import com.fprtsevenseven.notes.utils.SingleLiveEvent

class NotesFragment : Fragment() {

    private lateinit var binding: FragmentNotesBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val factory = InjectorUtils.provideNotesViewModelFactory(context!!)
        val viewModelFromFactory = ViewModelProviders.of(this, factory).get(NotesViewModel::class.java)

        binding = DataBindingUtil.inflate<FragmentNotesBinding>(
            inflater,
            R.layout.fragment_notes,
            container,
            false
        ).apply {
            viewModel = viewModelFromFactory
            lifecycleOwner = this@NotesFragment
        }

        val adapter = NotesAdapter(object : NotesAdapter.ItemsCallback{
            override fun registerEditObserver(
                expandingNoteModel: ExpandingNoteModel,
                editeNoteClick: SingleLiveEvent<Void>
            ) {
                editeNoteClick.observe(viewLifecycleOwner, Observer {
                    showNoteDialog(expandingNoteModel)
                })
            }

            override fun registerDeleteObserver(
                expandingNoteModel: ExpandingNoteModel,
                deleteNoteClick: SingleLiveEvent<Void>
            ) {
                deleteNoteClick.observe(viewLifecycleOwner, Observer {
                    binding.viewModel?.delete(expandingNoteModel.noteModel.id)
                })
            }
        })
        binding.recyclerView.layoutManager = GridLayoutManager(
            context,
            1,
            GridLayoutManager.VERTICAL,
            false
        )
        binding.recyclerView.adapter = adapter

        viewModelFromFactory.notes.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it.map { note -> ExpandingNoteModel(note, false) })
            adapter.notifyDataSetChanged()
        })

        viewModelFromFactory.newNoteClick.observe(viewLifecycleOwner, Observer {
            showNoteDialog()
        })

        return binding.root
    }

    fun refresh() {
        binding.viewModel?.refresh()
    }

    fun deleteAll() {
        binding.viewModel?.deleteAll()
    }

    fun collapseAll() {
        (binding.recyclerView.adapter as? NotesAdapter)?.collapseAll()
    }

    fun expandAll() {
        (binding.recyclerView.adapter as? NotesAdapter)?.expandAll()
    }

    private fun showNoteDialog(expandingNoteModel: ExpandingNoteModel? = null) {
        val builder = AlertDialog.Builder(context)
        val wrapper = FrameLayout(builder.context)
        wrapper.setPadding(builder.context.resources.getDimensionPixelSize(R.dimen.new_note_edit_text_padding))
        val editText = EditText(builder.context)
        editText.maxHeight = builder.context.resources.getDimensionPixelSize(R.dimen.new_note_edit_text_max_height)
        expandingNoteModel?.noteModel?.content?.let{ editText.setText(it) }
        wrapper.addView(
            editText, FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                Gravity.NO_GRAVITY
            )
        )
        builder.setView(wrapper)

        when (expandingNoteModel) {
            null -> {
                builder.setTitle(builder.context.resources.getString(R.string.dialog_title_create))
                builder.setNegativeButton(builder.context.resources.getString(R.string.dialog_button_cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                builder.setPositiveButton(builder.context.resources.getString(R.string.dialog_button_create)) { dialog, _ ->
                    binding.viewModel?.createNote(editText.text.toString())
                    dialog.dismiss()
                }
            }
            else -> {
                builder.setTitle(builder.context.resources.getString(R.string.dialog_title_edit))
                builder.setNegativeButton(builder.context.resources.getString(R.string.dialog_button_cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                builder.setPositiveButton(builder.context.resources.getString(R.string.dialog_button_save)) { dialog, _ ->
                    binding.viewModel?.updateNote(expandingNoteModel, editText.text.toString())
                    dialog.dismiss()
                }
            }
        }

        builder.create().show()
    }
}