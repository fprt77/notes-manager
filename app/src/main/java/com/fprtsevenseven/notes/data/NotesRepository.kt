package com.fprtsevenseven.notes.data

import androidx.lifecycle.MutableLiveData
import com.fprtsevenseven.basemodel.NotesApi
import com.fprtsevenseven.basemodel.data.NoteModel
import com.fprtsevenseven.notes.utils.Loader

class NotesRepository(
    private val api: NotesApi
) {
    fun getNotes(notesLD: MutableLiveData<List<NoteModel>>) {
        Loader.launchWithLoading(
            delay = 100 // just to skip loading progress in case when data can be prepared by NotesApi immediately
        ) {
            notesLD.postValue(loadNotes())
        }
    }

    private suspend fun loadNotes(): List<NoteModel> = api.listNotes()

    fun deleteAllNotes(notesLD: MutableLiveData<List<NoteModel>>) {
        Loader.launchWithLoading(
            delay = 100 // just to skip loading progress in case when data can be prepared by NotesApi immediately
        ) {
            notesLD.value?.forEach {
                api.deleteNote(it.id)
            }
            notesLD.postValue(emptyList())
        }
    }

    fun createNote(text: String, notesLD: MutableLiveData<List<NoteModel>>) {
        Loader.launchWithLoading(
            delay = 100 // just to skip loading progress in case when data can be prepared by NotesApi immediately
        ) {
            val result = api.addNote(NoteModel("", text))
            notesLD.postValue(notesLD.value?.toMutableList()?.apply { add(result) } ?: listOf(result))
        }
    }

    fun deleteNote(id: String, notesLD: MutableLiveData<List<NoteModel>>) {
        Loader.launchWithLoading(
            delay = 100 // just to skip loading progress in case when data can be prepared by NotesApi immediately
        ) {
            api.deleteNote(id)
            notesLD.postValue(notesLD.value?.toMutableList()?.filter { it.id != id } ?: emptyList() )
        }
    }

    fun updateNote(id: String, newContent: String, notesLD: MutableLiveData<List<NoteModel>>) {
        Loader.launchWithLoading(
            delay = 100 // just to skip loading progress in case when data can be prepared by NotesApi immediately
        ) {
            api.updateNote(
                NoteModel(
                    id = id,
                    content = newContent
                )
            )
            notesLD.postValue(notesLD.value?.toMutableList()?.apply { find { it.id == id }?.content = newContent } )
        }
    }

}