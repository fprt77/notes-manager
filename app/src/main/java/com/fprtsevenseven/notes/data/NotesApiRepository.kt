package com.fprtsevenseven.notes.data

import android.content.Context
import com.fprtsevenseven.basemodel.NotesApi
import com.fprtsevenseven.notes.Flavor

object NotesApiRepository {

    // For Singleton instantiation
    @Volatile
    private var instance: NotesApi? = null

    fun getNotesApi(context: Context) =
        instance ?: synchronized(this) {
            Flavor().createApi(context).also {
                instance = it
            }
        }

}