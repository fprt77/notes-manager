package com.fprtsevenseven.notes.data

import com.fprtsevenseven.basemodel.data.NoteModel

data class ExpandingNoteModel(
    var noteModel: NoteModel,
    var expanded: Boolean
)