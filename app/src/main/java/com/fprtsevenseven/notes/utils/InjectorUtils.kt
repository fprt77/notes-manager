package com.fprtsevenseven.notes.utils

import android.content.Context
import com.fprtsevenseven.basemodel.NotesApi
import com.fprtsevenseven.notes.data.NotesApiRepository
import com.fprtsevenseven.notes.data.NotesRepository
import com.fprtsevenseven.notes.viewmodels.MainActivityViewModelFactory
import com.fprtsevenseven.notes.viewmodels.NotesViewModelFactory

object InjectorUtils {

    /**
     * DI providers
     */

    fun provideMainActivityViewModelFactory() = MainActivityViewModelFactory()

    fun provideNotesViewModelFactory(context: Context) = NotesViewModelFactory(
        getNotesRepository(
            getNotesApi(context)
        )
    )

    //endregion

    /**
     * DI getters
     */

    private fun getNotesRepository(notesApi: NotesApi) = NotesRepository(notesApi)
    //endregion

    private fun getNotesApi(context: Context): NotesApi = NotesApiRepository.getNotesApi(context)

}