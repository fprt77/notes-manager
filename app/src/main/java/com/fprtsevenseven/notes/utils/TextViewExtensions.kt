package com.fprtsevenseven.notes.utils

import android.graphics.Paint
import android.widget.TextView

fun TextView.canBeSingleLine(inputText: String): Boolean {
    val measurePaint = Paint(paint)
    if (inputText.contains('\n')) return false
    val wholeWidth = measurePaint.measureText(inputText)
    return wholeWidth < width
}