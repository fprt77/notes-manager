package com.fprtsevenseven.notes.utils

import android.os.Handler
import android.os.Looper
import com.fprtsevenseven.basemodel.data.GeneralModelException
import com.fprtsevenseven.basemodel.data.GeneralServerException
import com.fprtsevenseven.basemodel.data.NetworkApiException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

object Loader {

    var loadingContract: LoadingContract? = null

    interface LoadingContract {
        fun setLoading(visible: Boolean, delay: Long = 0)
        fun showToast(message: String)
    }

    fun launchWithLoading(
        delay: Long = 0,
        block: suspend CoroutineScope.() -> Unit
    ) {
        CoroutineScope(Dispatchers.Main).launch {
            loadingContract?.setLoading(
                visible = true,
                delay = delay
            )
            val task = async(Dispatchers.IO) {
                try {
                    block()
                } catch (t: Throwable) {
                    t.printStackTrace()
                    processException(t)
                }
            }
            task.await()
            loadingContract?.setLoading(
                visible = false
            )
        }
    }

    private fun processException(e: Throwable) {
        when (e) {
            is NetworkApiException -> showToast("Server request was not successful. Please check your internet connectivity.")
            is GeneralModelException -> showToast("GeneralModelException message = ${e.message}")
            is GeneralServerException -> showToast(
                "GeneralServerException httpcode = ${e.httpCode} errcode = ${e.errorCode} message = ${e.message}"
            )
            else -> showToast("Unknown exception - $e")
        }
    }

    private fun showToast(message: String) {
        Handler(Looper.getMainLooper()).post {
            loadingContract?.showToast(message)
        }
    }

}