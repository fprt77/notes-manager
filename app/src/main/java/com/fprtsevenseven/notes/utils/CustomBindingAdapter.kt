package com.fprtsevenseven.notes.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("android:background")
fun ImageView.setBackground(resource: Int) {
    this.setBackgroundResource(resource)
}