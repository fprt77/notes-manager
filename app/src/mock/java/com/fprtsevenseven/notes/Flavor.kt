package com.fprtsevenseven.notes

import android.content.Context
import com.fprtsevenseven.basemodel.NotesApi
import com.fprtsevenseven.mockedmodel.MockedApi

class Flavor: FlavorBase() {

    override fun createApi(context: Context): NotesApi = MockedApi()

}