package com.fprtsevenseven.notes

import android.content.Context
import com.fprtsevenseven.basemodel.NotesApi
import com.fprtsevenseven.roommodel.RoomApi

class Flavor: FlavorBase() {

    override fun createApi(context: Context): NotesApi = RoomApi(context)

}