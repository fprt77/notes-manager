# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

##Retrofit
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions
-dontnote retrofit2.**

#Okhttp
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**
-dontnote okhttp3.**

# Okio
-dontnote okio.**

-dontnote android.databinding.**
-dontnote android.arch.lifecycle.**
-dontnote kotlin.**

# androidx
-dontwarn androidx.**
-dontnote androidx.**
-keep interface androidx.** { *; }
-keep class androidx.** { *; }

# google obfuscated code
-keep interface com.google.** { *; }
-keep class com.google.** { *; }